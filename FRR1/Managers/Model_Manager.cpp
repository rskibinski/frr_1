#include "Model_Manager.h"
#include <math.h>

int Models::PlyObject::c = 0;

Managers::Models_Manager::Models_Manager(std::string modelLocation)
{
	
	createObjectScene(modelLocation);
	glGenQueries(1, &queryId);

}

Managers::Models_Manager::~Models_Manager()
{
	for (auto model : gameModelList)
	{
		delete model;
	}
	gameModelList.clear();
}


void Managers::Models_Manager::createObjectScene(std::string modelLocation)
{
	bool renderObject = true;
	bool renderBSphere = false;
	bool frustumCulling = true; //TODO REMOVE IT

	glm::vec4 objectColor = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);// glm::vec4(1.0f, 0.5f, 0.31f, 1.0f);

	int program = Shader_Manager::GetShader("colorShader");
	glUseProgram(program);
	GLint lightColorId = glGetUniformLocation(program, "light_color");
	glUniform3f(lightColorId, 1.0f, 1.0f, 1.0f);

	GLint objectColorId = glGetUniformLocation(program, "object_color");
	glUniform3f(objectColorId, objectColor.r, objectColor.g, objectColor.b);

	int lightProgram = Shader_Manager::GetShader("lightShader");


	//create light
	Models::PlyObject *light = new Models::PlyObject("Rendering\\Models\\Cube.ply",
		glm::vec3(2.0, 2.0, 0.0),
		glm::vec3(0.3f),
		glm::vec4(1.0f, 1.0f, 1.0f, 1.0f),
		GL_TRIANGLES,
		renderBSphere,
		renderObject,
		frustumCulling);
	light->setProgram(lightProgram);
	light->create();
	gameModelList.push_back(light);


	//create cube
	program = Shader_Manager::GetShader("colorShader");
	Models::PlyObject *object = new Models::PlyObject(modelLocation,
		glm::vec3(0.0, 0.0, 0.0),
		glm::vec3(1.0f),
		objectColor,
		GL_TRIANGLES,
		renderBSphere,
		renderObject,
		frustumCulling);

	object->setProgram(program);
	object->create();
	gameModelList.push_back(object);

}



void Managers::Models_Manager::Draw(GLuint shader)
{
	int i = 0;
	Models::PlyObject::c = 0;
	switch (drawingMode)
	{
	case(NORMAL):
		{
			for (auto model : gameModelList)
			{
				model->setProgram(shader);
				model->draw();
			}break;
		}
	case(FRUSTUM_CULLING):
		{
			Models::PlyObject::c = 0;
			glm::mat4 VP = Managers::Controls::getProjectionMatrix() * Managers::Controls::getViewMatrix();
			Managers::ViewFrustum *vf = Managers::Controls::getViewFrustum(VP);
			for (auto model : gameModelList)
			{
				model->computeIfInFrustum(vf);
			}
			for (auto model : gameModelList)
			{
				if (model->isInFrustum())
				{
					model->setProgram(shader);
					model->draw();
					Models::PlyObject::c++;
				}

			}
			std::cout <<"Models: " << Models::PlyObject::c << "\n";
			break;
		}

	case(OCCLUSION_QUERY):
		{
			Models::PlyObject::c = 0;
			glColorMask(false, false, false, false);
			glDepthMask(GL_FALSE);
			for (auto model : gameModelList)
			{
				model->runOcclusionQuery();
			}
			glColorMask(true, true, true, true);
			glDepthMask(GL_TRUE);
			for (auto model : gameModelList)
			{
				//std::cout << model->getSamplesNumber() << "\n";
				if (model->getSamplesNumber() > 0)
				{
					model->setProgram(shader);
					model->draw();
					Models::PlyObject::c++;
				}
				
			}
			std::cout <<"Models: " << Models::PlyObject::c << "\n";
			break;
		}
	}
	

}

void Managers::Models_Manager::Update()
{
	for (auto model : gameModelList)
	{
		model->update();
	}
}



