#pragma once

#include "Model_Manager.h"
#include "Shader_Manager.h"
#include "..\Core\Init\IListener.h"

namespace Managers
{
	class Scene_Manager: public Core::IListener
	{
	public:
		Scene_Manager(std::string modelLocation);
		~Scene_Manager();
		virtual void notifyBeginFrame();
		virtual void notifyDisplayFrame();
		void ssaoDraw();
		void geometryPass(int gBuffer, GLuint geometryShader);
		void ssaoPass();
		void blurPass();
		void lightningPass();
		virtual void notifyEndFrame();
		virtual void notifyReshape(int width,
			int height,
			int previous_width,
			int previous_height);
		const static glm::vec2 getScreenSize();
		const static float getNearPlan();
		const static float getFarPlan();
	private:
		Managers::Models_Manager *modelsManager;
		Managers::Shader_Manager *shaderManager;

	};
}