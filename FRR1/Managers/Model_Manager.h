#pragma once
#include <map>
#include "Shader_Manager.h"
#include "../Rendering/IGameObject.h"
#include "../Rendering/Models/PlyObject.h"
#include "../Dependencies/trimesh/TriMesh.h"

using namespace Rendering;
namespace Managers
{
	enum DrawingMode
	{
		NORMAL,
		FRUSTUM_CULLING,
		OCCLUSION_QUERY
	};

	class Models_Manager
	{
	public:
		Models_Manager(std::string modelLocation);
		~Models_Manager();

		void createObjectScene(std::string);

		void Draw(GLuint shader);
		void Update();
		DrawingMode drawingMode = NORMAL;
	private:
		std::vector<IGameObject*> gameModelList;
		GLuint queryId;
	};
}