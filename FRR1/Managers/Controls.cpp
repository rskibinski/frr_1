#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtx\string_cast.hpp"
#include "glm\gtc\type_ptr.hpp"

#include "Scene_Manager.h"


#include "controls.h"

glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;
glm::vec3 cameraPosition;
double Managers::Controls::time = 0;
Managers::FINAL_MODE  Managers::Controls::drawingMode = SSAO;

float Managers::Controls::farPlane = 20.0;
float Managers::Controls::nearPlane = 0.1;
float Managers::Controls::kernelRadius = 10.0;


glm::mat4 Managers::Controls::getViewMatrix() {
	return ViewMatrix;
}
glm::mat4 Managers::Controls::getProjectionMatrix() {
	return ProjectionMatrix;
}
glm::vec3 Managers::Controls::getCameraPosition() {
	return cameraPosition;
}

Managers::ViewFrustum * Managers::Controls::getViewFrustum(glm::mat4 P)
{
		Managers::ViewFrustum *vf = new Managers::ViewFrustum();

		vf->planes[LEFT][0] = P[0][3] + P[0][0];
		vf->planes[LEFT][1] = P[1][3] + P[1][0];
		vf->planes[LEFT][2] = P[2][3] + P[2][0];
		vf->planes[LEFT][3] = P[3][3] + P[3][0];

		vf->planes[RIGHT][0] = P[0][3] - P[0][0];
		vf->planes[RIGHT][1] = P[1][3] - P[1][0];
		vf->planes[RIGHT][2] = P[2][3] - P[2][0];
		vf->planes[RIGHT][3] = P[3][3] - P[3][0];

		vf->planes[TOP][0] = P[0][3] - P[0][1];
		vf->planes[TOP][1] = P[1][3] - P[1][1];
		vf->planes[TOP][2] = P[2][3] - P[2][1];
		vf->planes[TOP][3] = P[3][3] - P[3][1];

		vf->planes[BOTTOM][0] = P[0][3] + P[0][1];
		vf->planes[BOTTOM][1] = P[1][3] + P[1][1];
		vf->planes[BOTTOM][2] = P[2][3] + P[2][1];
		vf->planes[BOTTOM][3] = P[3][3] + P[3][1];

		vf->planes[NNEAR][0] = P[0][3] + P[0][2];
		vf->planes[NNEAR][1] = P[1][3] + P[1][2];
		vf->planes[NNEAR][2] = P[2][3] + P[2][2];
		vf->planes[NNEAR][3] = P[3][3] + P[3][2];

		vf->planes[FFAR][0] = P[0][3] - P[0][2];
		vf->planes[FFAR][1] = P[1][3] - P[1][2];
		vf->planes[FFAR][2] = P[2][3] - P[2][2];
		vf->planes[FFAR][3] = P[3][3] - P[3][2];


		//normalize

		float mag;
		for (int i = 0; i < 6; i++)
		{
			mag = sqrt(vf->planes[i][0] * vf->planes[i][0] +
				vf->planes[i][1] * vf->planes[i][1] +
				vf->planes[i][2] * vf->planes[i][2]);
			vf->planes[i][0] /= mag;
			vf->planes[i][1] /= mag;
			vf->planes[i][2] /= mag;
			vf->planes[i][3] /= mag;
		}

		return vf;
}




// Initial position : on +Z
glm::vec3 position = glm::vec3(0, 0, 10);
// Initial horizontal angle : toward -Z
float horizontalAngle = 3.14f;
// Initial vertical angle : none
float verticalAngle = 0.0f;
// Initial Field of View
float initialFoV = 45.0f;

float speed = 3.0f; // 3 units / second
float mouseSpeed = 0.005f;


double Managers::Controls::getTime() {
	return time;
}

const float Managers::Controls::getKernelRadius() {
	return Managers::Controls::kernelRadius;
}


void Managers::Controls::computeMatricesFromInputs() {

	// glfwGetTime is called only once, the first time this function is called
	static double lastTime = glfwGetTime();

	// Compute time difference between current and last frame
	double currentTime = glfwGetTime();
	time = currentTime;
	float deltaTime = float(currentTime - lastTime);

	// Get mouse position
	GLFWwindow *window = Core::Init_GLFW::getWindow();
	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	const Core::WindowInfo *wInfo = Core::Init_GLFW::getWindowInfo();
	// Reset mouse position for next frame
	glfwSetCursorPos(window, wInfo->width/2, wInfo->height/2);

	// Compute new orientation
	horizontalAngle += mouseSpeed * float(wInfo->width / 2 - xpos);
	verticalAngle += mouseSpeed * float(wInfo->height / 2 - ypos);

	// Direction : Spherical coordinates to Cartesian coordinates conversion
	glm::vec3 direction(
		cos(verticalAngle) * sin(horizontalAngle),
		sin(verticalAngle),
		cos(verticalAngle) * cos(horizontalAngle)
	);

	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horizontalAngle - 3.14f / 2.0f),
		0,
		cos(horizontalAngle - 3.14f / 2.0f)
	);

	// Up vector
	glm::vec3 up = glm::cross(right, direction);

	// Move forward
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		position += direction * deltaTime * speed;
	}
	// Move backward
	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
		position -= direction * deltaTime * speed;
	}
	// Strafe right
	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		position += right * deltaTime * speed;
	}
	// Strafe left
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
		position -= right * deltaTime * speed;
	}

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		kernelRadius += 1.0;
	}
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
	{
		kernelRadius = glm::max(1.0, kernelRadius-1.0);
	}
	if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
	{
		drawingMode = BLUR;
	}
	if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS)
	{
		drawingMode = SSAO;
	}
	if (glfwGetKey(window, GLFW_KEY_G) == GLFW_PRESS)
	{
		drawingMode = GEOMETRY;
	}




	float FoV = initialFoV;

						   // Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	ProjectionMatrix = glm::perspective(FoV, 4.0f / 3.0f, Managers::Scene_Manager::getNearPlan()
		, Managers::Scene_Manager::getFarPlan());

	// Camera matrix
	ViewMatrix = glm::lookAt(
		position,           // Camera is here
		position + direction, // and looks here : at the same position, plus "direction"
		up                  // Head is up (set to 0,-1,0 to look upside-down)
	);
	cameraPosition = position;



	// For the next frame, the "last time" will be "now"
	lastTime = currentTime;
}

Managers::ViewFrustum::~ViewFrustum()
{
}

const Managers::FINAL_MODE Managers::Controls::getDrawingMode() {
	return drawingMode;
}

bool Managers::ViewFrustum::isSphereInFrustum(trimesh::point point, float radius)
{
	float distance;
	for (int i = 0; i < 6; i++)
	{
		distance = signed_d(i, point);
		if (distance - radius < 0)
			return false;
	}
	return true;
}

float Managers::ViewFrustum::signed_d(int plane, trimesh::point point)
{
	return planes[plane][0] * point[0] + planes[plane][1] * point[1] + planes[plane][2] * point[2] + planes[plane][3];
}
