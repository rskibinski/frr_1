#pragma once
#include "../Core/Init/Init_GLFW.h"
#include "../Dependencies/trimesh/TriMesh.h"

#define LEFT 0
#define RIGHT 1
#define TOP 2
#define BOTTOM 3
#define NNEAR 4
#define FFAR 5

namespace Managers
{
	enum FINAL_MODE { GEOMETRY, BLUR, SSAO };

	class ViewFrustum
	{
	public:
		~ViewFrustum();
		bool isSphereInFrustum(trimesh::point point, float radius);
		float signed_d(int plane, trimesh::point point);
		float planes[6][4];
	};

	class Controls {

	public:
		static void computeMatricesFromInputs();
		static glm::mat4 getViewMatrix();
		static glm::mat4 getProjectionMatrix();
		static Managers::ViewFrustum *getViewFrustum(glm::mat4);
		static glm::vec3 getCameraPosition();
		static double getTime();
		const static float getKernelRadius();
		const static FINAL_MODE getDrawingMode();

		static float farPlane, nearPlane, kernelRadius;
		static FINAL_MODE drawingMode;

	private:
		static double time;
	};

	

}