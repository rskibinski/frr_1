#include "Scene_Manager.h"
#include <random>
using namespace Managers;

int drawing_mode = SSAO;

const float X = 800;
const float Y = 600;
const int NOISE_SIZE = 4;

void bindVPMatrices(GLuint);
void initializeGBuffer();
void initializeSSAOBuffer();

GLuint gBuffer, ssaoFBO, ssaoBlurFBO;
GLuint gPositionDepth, gNormal, gAlbedo;

GLuint ssaoColorBuffer, ssaoColorBufferBlur;

GLuint quadVAO;
std::vector<glm::vec3> ssaoKernel;
GLuint noiseTexture;


GLfloat quadVertices[] = {   // Vertex attributes for a quad that fills the entire screen in Normalized Device Coordinates.
							 // Positions   // TexCoords
	-1.0f,  1.0f,  0.0f, 1.0f,
	-1.0f, -1.0f,  0.0f, 0.0f,
	1.0f, -1.0f,  1.0f, 0.0f,

	-1.0f,  1.0f,  0.0f, 1.0f,
	1.0f, -1.0f,  1.0f, 0.0f,
	1.0f,  1.0f,  1.0f, 1.0f
};

void createQuad()
{
	GLuint quadVBO;
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glBindVertexArray(0);
}
void drawQuad()
{
	glBindVertexArray(quadVAO);
	glDisable(GL_DEPTH_TEST);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

Scene_Manager::Scene_Manager(std::string modelLocation)
{
	glEnable(GL_CULL_FACE);

	shaderManager = new Shader_Manager();
	shaderManager->CreateProgram("colorShader",
		"Shaders\\GeometryVertex.glsl",
		"Shaders\\FragmentShader.glsl");

	shaderManager->CreateProgram("lightShader",
		"Shaders\\GeometryVertex.glsl",
		"Shaders\\LightFragmentShader.glsl");

	shaderManager->CreateProgram("geometryShader",
		"Shaders\\GeometryVertex.glsl",
		"Shaders\\GeometryFragment.glsl");

	GLuint ssaoShader = shaderManager->CreateProgram("ssaoShader",
		"Shaders\\SSAOVertex.glsl",
		"Shaders\\SSAOFragment.glsl");
	glUseProgram(ssaoShader);
	glUniform1i(glGetUniformLocation(ssaoShader, "gPositionDepth"), 0);
	glUniform1i(glGetUniformLocation(ssaoShader, "gNormal"), 1);
	glUniform1i(glGetUniformLocation(ssaoShader, "texNoise"), 2);

	GLuint ssaoBlurShader = shaderManager->CreateProgram("ssaoBlurShader",
		"Shaders\\BlurVertex.glsl",
		"Shaders\\BlurFragment.glsl");
	glUseProgram(ssaoBlurShader);
	glUniform1i(glGetUniformLocation(ssaoBlurShader, "ssaoInput"), 0);

	shaderManager->CreateProgram("testShader",
		"Shaders\\TestVertex.glsl",
		"Shaders\\TestFragment.glsl");

	modelsManager = new Models_Manager(modelLocation);
	initializeGBuffer();
	initializeSSAOBuffer();
	createQuad();
}

void initializeGBuffer()
{
	int SCR_WIDTH = X;
	int SCR_HEIGHT = Y;

	glGenFramebuffers(1, &gBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
	// - Position + linear depth color buffer
	glGenTextures(1, &gPositionDepth);
	glBindTexture(GL_TEXTURE_2D, gPositionDepth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, gPositionDepth, 0);
	// - Normal color buffer
	glGenTextures(1, &gNormal);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, gNormal, 0);
	// - Albedo color buffer
	glGenTextures(1, &gAlbedo);
	glBindTexture(GL_TEXTURE_2D, gAlbedo);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, gAlbedo, 0);
	// - Tell OpenGL which color attachments we'll use (of this framebuffer) for rendering 
	GLuint attachments[3] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, attachments);
	// - Create and attach depth buffer (renderbuffer)
	GLuint rboDepth;
	glGenRenderbuffers(1, &rboDepth);
	glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, SCR_WIDTH, SCR_HEIGHT);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);
	// - Finally check if framebuffer is complete
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "GBuffer Framebuffer not complete!" << std::endl;
	}
	else {
		std::cout << "GBuffer Framebuffer completed!" << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GLfloat lerp(GLfloat a, GLfloat b, GLfloat f)
{
	return a + f * (b - a);
}

void initializeSSAOBuffer()
{
	int SCR_WIDTH = X;
	int SCR_HEIGHT = Y;


	glGenFramebuffers(1, &ssaoFBO);  
	glGenFramebuffers(1, &ssaoBlurFBO);
	
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	// - SSAO color buffer
	glGenTextures(1, &ssaoColorBuffer);
	glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBuffer, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "SSAO Framebuffer not complete!" << std::endl;
	// - and blur stage
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
	glGenTextures(1, &ssaoColorBufferBlur);
	glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "SSAO Blur Framebuffer not complete!" << std::endl;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	// Sample kernel
	std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // generates random floats between 0.0 and 1.0
	std::default_random_engine generator;
	for (GLuint i = 0; i < 64; ++i)
	{
		glm::vec3 sample(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, randomFloats(generator));
		sample = glm::normalize(sample);
		sample *= randomFloats(generator);
		GLfloat scale = GLfloat(i) / 64.0;

		// Scale samples s.t. they're more aligned to center of kernel
		scale = lerp(0.1f, 1.0f, scale * scale);
		sample *= scale;
		ssaoKernel.push_back(sample);
	}

	// Noise texture
	std::vector<glm::vec3> ssaoNoise;
	for (GLuint i = 0; i < NOISE_SIZE * NOISE_SIZE; i++)
	{
		glm::vec3 noise(randomFloats(generator) * 2.0 - 1.0, randomFloats(generator) * 2.0 - 1.0, 0.0f); // rotate around z-axis (in tangent space)
		ssaoNoise.push_back(noise);
	}
	glGenTextures(1, &noiseTexture);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, NOISE_SIZE, NOISE_SIZE, 0, GL_RGB, GL_FLOAT, &ssaoNoise[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

Scene_Manager::~Scene_Manager()
{
	delete shaderManager;
}

void Scene_Manager::notifyBeginFrame()
{
	//nothing here for the moment
}

void Scene_Manager::notifyDisplayFrame()
{
	glClearColor(0.0, 0.0, 0.6f, 1.0);
	Managers::Controls::computeMatricesFromInputs();
	bindVPMatrices(Managers::Shader_Manager::GetShader("colorShader"));
	if (drawing_mode == NORMAL)
	{
		glEnable(GL_DEPTH_TEST);
		modelsManager->Draw(Managers::Shader_Manager::GetShader("colorShader"));
	}
	else if (drawing_mode == SSAO) {
		ssaoDraw();
	}
}

void Scene_Manager::ssaoDraw()
{
	
	Managers::FINAL_MODE mode = Controls::getDrawingMode();
	switch (mode)
	{
	case GEOMETRY:
		geometryPass(0, Managers::Shader_Manager::GetShader("colorShader"));
		break;
	default:
		geometryPass(gBuffer, Managers::Shader_Manager::GetShader("geometryShader"));
		ssaoPass();
		blurPass();
		lightningPass();

	}

	}

void Scene_Manager::geometryPass(int gBuffer, GLuint geometryShader)
{
	glEnable(GL_DEPTH_TEST);
	glBindFramebuffer(GL_FRAMEBUFFER, gBuffer);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(geometryShader);

	glUniform1f(glGetUniformLocation(geometryShader, "NEAR"), getNearPlan());
	glUniform1f(glGetUniformLocation(geometryShader, "FAR"), getFarPlan());

	bindVPMatrices(geometryShader);
	modelsManager->Draw(geometryShader);

	glBindBuffer(GL_FRAMEBUFFER, 0);
}

void Scene_Manager::ssaoPass()
{
	glBindFramebuffer(GL_FRAMEBUFFER, ssaoFBO);
	glClear(GL_COLOR_BUFFER_BIT);
	GLuint ssaoShader = Managers::Shader_Manager::GetShader("ssaoShader");
	glUseProgram(ssaoShader);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, gPositionDepth);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, gNormal);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);
	for (GLuint i = 0; i < 64; ++i)
		glUniform3fv(glGetUniformLocation(ssaoShader, ("samples[" + std::to_string(i) + "]").c_str()), 1, &ssaoKernel[i][0]);
	glUniformMatrix4fv(glGetUniformLocation(ssaoShader, "projection"), 1, GL_FALSE, &(Controls::getProjectionMatrix()[0][0]));
	glUniform1f(glGetUniformLocation(ssaoShader, "radius"), Controls::getKernelRadius());

	glBindVertexArray(quadVAO);
	glDisable(GL_DEPTH_TEST);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);

	glBindBuffer(GL_FRAMEBUFFER, 0);

}

void Scene_Manager::blurPass()
{
	int noiseDimensionValue = (NOISE_SIZE % 2 == 1 ? (NOISE_SIZE + 1) / 2 : NOISE_SIZE / 2);
	GLuint noiseDimensionUniform;

	glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
	glClear(GL_COLOR_BUFFER_BIT);
	GLuint ssaoBlurShader = Managers::Shader_Manager::GetShader("ssaoBlurShader");
	glUseProgram(ssaoBlurShader);


	glUniform1i(glGetUniformLocation(ssaoBlurShader, "noiseDimension"), noiseDimensionValue);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);

	drawQuad();
}

void Scene_Manager::lightningPass()
{
	glDisable(GL_DEPTH_TEST);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT);
	GLuint testShader = Managers::Shader_Manager::GetShader("testShader");
	glUseProgram(testShader);

	glActiveTexture(GL_TEXTURE0);
	Managers::FINAL_MODE mode = Controls::getDrawingMode();
	switch (mode) {
	case SSAO:
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ssaoColorBuffer);
		drawQuad();
		break;
	case BLUR:
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
		drawQuad();
		break;
	}
	
}



void bindVPMatrices(GLuint program)
{
	GLuint V_MatrixID = glGetUniformLocation(program, "V");
	GLuint P_MatrixID = glGetUniformLocation(program, "P");
	GLuint CameraPosID = glGetUniformLocation(program, "camera_pos");

	glm::mat4 ProjectionMatrix = Managers::Controls::getProjectionMatrix();
	glm::mat4 ViewMatrix = Managers::Controls::getViewMatrix();
	glm::vec3 cameraPosition = Managers::Controls::getCameraPosition();

	glUniformMatrix4fv(V_MatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
	glUniformMatrix4fv(P_MatrixID, 1, GL_FALSE, &ProjectionMatrix[0][0]);
	glUniform3f(CameraPosID, cameraPosition.x, cameraPosition.y, cameraPosition.z);

	GLint lightPosId = glGetUniformLocation(program, "light_pos");
	double t = Managers::Controls::getTime();
	float x = 2.0f + sin(t)*2.0f;
	float y = 2.0f + cos(t)*2.0f;
	glUniform3f(lightPosId, x, y, 2.0f);
}

void Scene_Manager::notifyEndFrame()
{
	int fps = 0;
	fps++;
	if (glfwGetTime() - Core::Init_GLFW::time >= 1.0)
	{
		double radius = Managers::Controls::getKernelRadius();
		printf("Radius: [Q+] %f [A-]; %f fps\t[N]ormal SSAO, [G]eometry, [B]lur\n", double(radius), double(fps));
		Core::Init_GLFW::time += 1.0;
		fps = 0;
	}
}

void Scene_Manager::notifyReshape(int width,
	int height,
	int previous_width,
	int previous_height)
{
	//nothing here for the moment 

}

const glm::vec2 Scene_Manager::getScreenSize() {
	return glm::vec2(X, Y);
}

const float Scene_Manager::getNearPlan() {
	return Controls::nearPlane;
}

const float Scene_Manager::getFarPlan() {
	return Controls::farPlane;
}