#version 450

uniform sampler2D ColorBuffer, NormalBuffer, DepthBuffer;
uniform vec2 PixelSize;

vec2 Offsets[8] = vec2[](
    vec2(-1.0,  1.0),
    vec2( 0.0,  1.0),
    vec2( 1.0,  1.0),
    vec2( 1.0,  0.0),
    vec2( 1.0, -1.0),
    vec2( 0.0, -1.0),
    vec2(-1.0, -1.0),
    vec2(-1.0,  0.0)
);

void main()
{
    vec2 TexCoords[8];

    for(int i = 0; i < 8; i++)
    {  
        TexCoords[i] = gl_TexCoord[0].st + Offsets[i] * PixelSize;
    }

    float Depth = texture2D(DepthBuffer, gl_TexCoord[0].st).r;

    vec4 Depths1, Depths2;

    Depths1.x = texture2D(DepthBuffer, TexCoords[0]).r;
    Depths1.y = texture2D(DepthBuffer, TexCoords[1]).r;
    Depths1.z = texture2D(DepthBuffer, TexCoords[2]).r;
    Depths1.w = texture2D(DepthBuffer, TexCoords[3]).r;
    Depths2.x = texture2D(DepthBuffer, TexCoords[4]).r;
    Depths2.y = texture2D(DepthBuffer, TexCoords[5]).r;
    Depths2.z = texture2D(DepthBuffer, TexCoords[6]).r;
    Depths2.w = texture2D(DepthBuffer, TexCoords[7]).r;

    vec4 DepthDeltas1 = abs(Depths1 - Depth);
    vec4 DepthDeltas2 = abs(Depth - Depths2);

    vec4 MinDepthDeltas = max(min(DepthDeltas1, DepthDeltas2), 0.00001);
    vec4 MaxDepthDeltas = max(DepthDeltas1, DepthDeltas2);

    vec4 DepthResults = step(MinDepthDeltas * 25.0, MaxDepthDeltas);

    vec3 Normal = normalize(texture2D(NormalBuffer, gl_TexCoord[0].st).rgb * 2.0 - 1.0);

    vec4 Dots1, Dots2;

    Dots1.x = dot(normalize(texture2D(NormalBuffer, TexCoords[0]).rgb * 2.0 - 1.0), Normal);         
    Dots1.y = dot(normalize(texture2D(NormalBuffer, TexCoords[1]).rgb * 2.0 - 1.0), Normal);
    Dots1.z = dot(normalize(texture2D(NormalBuffer, TexCoords[2]).rgb * 2.0 - 1.0), Normal);
    Dots1.w = dot(normalize(texture2D(NormalBuffer, TexCoords[3]).rgb * 2.0 - 1.0), Normal);
    Dots2.x = dot(normalize(texture2D(NormalBuffer, TexCoords[4]).rgb * 2.0 - 1.0), Normal);
    Dots2.y = dot(normalize(texture2D(NormalBuffer, TexCoords[5]).rgb * 2.0 - 1.0), Normal);
    Dots2.z = dot(normalize(texture2D(NormalBuffer, TexCoords[6]).rgb * 2.0 - 1.0), Normal);
    Dots2.w = dot(normalize(texture2D(NormalBuffer, TexCoords[7]).rgb * 2.0 - 1.0), Normal);

    vec4 DotDeltas = abs(Dots1 - Dots2);

    vec4 NormalResults = step(0.4, DotDeltas);

    vec4 Results = max(NormalResults, DepthResults);

    float EdgeWeight = (Results.x + Results.y + Results.z + Results.w) * 0.25;

    if(EdgeWeight > 0.0)
    {
        vec3 Color = texture2D(ColorBuffer, gl_TexCoord[0].st).rgb;
        vec3 ColorsSum = vec3(0.0);

        for(int i = 0; i < 8; i++)
        {
            ColorsSum += texture2D(ColorBuffer, TexCoords[i]).rgb;
        }

        gl_FragColor = vec4(mix(Color, ColorsSum * 0.125, EdgeWeight), 1.0);
    }
    else
    {
        gl_FragColor = texture2D(ColorBuffer, gl_TexCoord[0].st);
    }
}