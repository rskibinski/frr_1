#version 450 core
in vec2 TexCoords;
out float fragColor;

uniform sampler2D ssaoInput;
uniform int noiseDimension;

void main() {
    vec2 texelSize = 1.0 / vec2(textureSize(ssaoInput, 0));
    float result = 0.0;
    for (int x = -noiseDimension; x < noiseDimension; ++x) 
    {
        for (int y = -noiseDimension; y < noiseDimension; ++y) 
        {
            vec2 offset = vec2(float(x), float(y)) * texelSize;
            result += texture(ssaoInput, TexCoords + offset).r;
        }
    }
    fragColor = result / (noiseDimension * noiseDimension * 4.0);
}  