#version 450 core
 
in vec3 normal;
in vec3 fragment_pos;

out vec4 color;

uniform vec3 camera_pos;
uniform vec3 object_color;
uniform vec3 light_color;
uniform vec3 light_pos;
 
void main()
{
	vec3 norm = normalize(normal);
	vec3 light_dir = normalize(light_pos - fragment_pos);

	float diff = max(dot(norm, light_dir), 0.0);
	vec3 diffuse = diff * light_color;

	float ambientStrength =	0.1f;
	vec3 ambient = ambientStrength * light_color;

	float specularStrength = 1.0f;
	vec3 viewDir = normalize(camera_pos - fragment_pos);
	vec3 reflectDir = reflect(-light_dir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = specularStrength * spec * light_color;

	vec3 result = (ambient + diffuse + specular) * object_color;
	color = vec4(result, 1.0f);
}