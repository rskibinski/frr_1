#version 450 core 

layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec3 in_texCoords;

out vec3 fragment_pos;
out vec3 normal;

uniform mat4 M;
uniform mat4 V;
uniform mat4 P;
 
void main()
{

  vec4 VM = V * M  * vec4(in_position, 1.0f); //view pos
  fragment_pos = VM.xyz;
  gl_Position = P * VM;

  mat3 normalMatrix = transpose(inverse(mat3(V * M)));
  normal = normalMatrix * in_normal;
}