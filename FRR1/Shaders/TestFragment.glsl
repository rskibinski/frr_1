#version 450 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D screenTexture;

void main()
{ 
    //color = texture(screenTexture, TexCoords);
	vec4 c = texture(screenTexture, TexCoords);
	color = vec4(c.x, c.x, c.x, 1.0f);
}