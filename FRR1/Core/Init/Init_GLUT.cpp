#include "Init_GLUT.h"

Core::IListener *Core::Init_GLUT::listener = NULL;

void Core::Init_GLUT::init(int argc, char **argv,
	const Core::WindowInfo &window, 
	const Core::ContextInfo &context, 
	const Core::FramebufferInfo &framebuffer)
{
	glutInit(&argc, argv);

	if (context.core)
	{
		glutInitContextVersion(context.major,
			context.minor);
		glutInitContextProfile(GLUT_CORE_PROFILE);
	}
	else
	{
		glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);
	}

	glutInitDisplayMode(framebuffer.flags);
	glutInitWindowPosition(window.pos_x, window.pos_y);
	glutInitWindowSize(window.width, window.height);

	glutCreateWindow(window.name.c_str());

	glutIdleFunc(idleCallback);
	glutCloseFunc(closeCallback);
	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);

	Core::Init_GLEW::init();

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
}

void Core::Init_GLUT::run()
{
	std::cout << "GLUT:\t Starting main loop \n";
	glutMainLoop();
}

void Core::Init_GLUT::close()
{
	close();
}

void Core::Init_GLUT::setListener(Core::IListener *& iListener)
{
	listener = iListener;
}

void Core::Init_GLUT::idleCallback(void)
{
}

void Core::Init_GLUT::displayCallback(void)
{
	if (listener) {
		listener->notifyBeginFrame();
		listener->notifyDisplayFrame();

		glutSwapBuffers();

		listener->notifyEndFrame();
	}
	
}

void Core::Init_GLUT::reshapeCallback(int width, int height)
{
}

void Core::Init_GLUT::closeCallback(void)
{
}
