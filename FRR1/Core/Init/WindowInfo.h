#pragma once
#include <string>
#include "../../Dependencies/glew/glew.h"
#include "../../Dependencies/freeglut/freeglut.h"

namespace Core
{
	struct WindowInfo
	{
		std::string name;
		int width, height;
		int pos_x, pos_y;
		bool isReshapable = true;

		WindowInfo(std::string name,
			int width, int height,
			int pos_x, int pos_y,
			bool isReshapable = true)
		{
			this->name = name;
			this->width = width;
			this->height = height;
			this->pos_x = pos_x;
			this->pos_y = pos_y;
			this->isReshapable = isReshapable;
		}
		
	};

	struct ContextInfo
	{
		int major = 3;
		int minor = 3;
		bool core = true;

		ContextInfo(int major, int minor, bool core)
		{
			this->major = major;
			this->minor = minor;
			this->core = core;
		}
	};

	struct FramebufferInfo {

		unsigned int flags;
		bool msaa;

		FramebufferInfo()
		{
			flags = GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH;
			msaa = false;
		}

		FramebufferInfo(bool color, bool depth, bool stencil, bool msaa)
		{
			flags = GLUT_DOUBLE; 
			if (color)
				flags |= GLUT_RGBA | GLUT_ALPHA;
			if (depth)
				flags |= GLUT_DEPTH;
			if (stencil)
				flags |= GLUT_STENCIL;
			if (msaa)
				flags |= GLUT_MULTISAMPLE;
			this->msaa = msaa;
		}

	};
}