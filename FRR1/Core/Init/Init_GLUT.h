#pragma once
#include <iostream>
#include "Init_GLEW.h"

#include "WindowInfo.h"

#include "../../Dependencies/freeglut/freeglut.h"
#include "IListener.h"

namespace Core
{
	class Init_GLUT
	{
	public:
		static void init(int argc, char **argv,
			const Core::WindowInfo &window,
			const Core::ContextInfo &context,
			const Core::FramebufferInfo &framebuffer);
		
		static void run();
		static void close();
		static void setListener(Core::IListener*& iListener);

	private:
		static void idleCallback(void);
		static void displayCallback(void);
		static void reshapeCallback(int width, int height);
		static void closeCallback(void);

		static Core::IListener* listener;
	};
}