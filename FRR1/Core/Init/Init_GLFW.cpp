#include "Init_GLFW.h"


Core::IListener *Core::Init_GLFW::listener = NULL;
GLFWwindow *Core::Init_GLFW::glfwWindow = NULL;
Core::WindowInfo const *Core::Init_GLFW::windowInfo = NULL;
double Core::Init_GLFW::time = 0;

void Core::Init_GLFW::init(int argc, char ** argv, const Core::WindowInfo & window, const Core::ContextInfo & context, const Core::FramebufferInfo &framebuffer)
{
	windowInfo = &window;
	if (!glfwInit())
	{
		exit(-1);
	}
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	if (framebuffer.msaa)
	{
		glfwWindowHint(GLFW_SAMPLES, 4);

	}

	glfwWindow = glfwCreateWindow(window.width, window.height, window.name.c_str(), NULL, NULL);
	if (glfwWindow == NULL)
	{
		fprintf(stderr, "Failed to open GLFW window\n");
		getchar();
		glfwTerminate();
		exit(-1);
	}
	glfwMakeContextCurrent(glfwWindow);


	//controls
	glfwSetInputMode(glfwWindow, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetInputMode(glfwWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwPollEvents();
	glfwSetCursorPos(glfwWindow, window.width / 2, window.height / 2);
	//

	Core::Init_GLEW::init();
}

void Core::Init_GLFW::run()
{
	time = glfwGetTime();
	do {


		if (listener)
		{
			listener->notifyBeginFrame();
			listener->notifyDisplayFrame();
			
			glfwSwapBuffers(glfwWindow);
			glfwPollEvents();

			listener->notifyEndFrame();
		}

	} while (glfwGetKey(glfwWindow, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
			glfwWindowShouldClose(glfwWindow) == 0);
}

void Core::Init_GLFW::close()
{
}

void Core::Init_GLFW::setListener(Core::IListener *& iListener)
{
	listener = iListener;
}

GLFWwindow * Core::Init_GLFW::getWindow()
{
	return glfwWindow;
}

Core::WindowInfo const *Core::Init_GLFW::getWindowInfo()
{
	return windowInfo;
}

void Core::Init_GLFW::idleCallback(void)
{
}

void Core::Init_GLFW::displayCallback(void)
{
}

void Core::Init_GLFW::reshapeCallback(int width, int height)
{
}

void Core::Init_GLFW::closeCallback(void)
{
}
