#include "Init_GLEW.h"

using namespace Core;

void Init_GLEW::init()
{
	glewExperimental = true;
	if (glewInit() == GLEW_OK)
	{
		std::cout << "GLEW Initialized\n";
	}
}

