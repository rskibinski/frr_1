#pragma once
#include "Init_GLEW.h"
#include "../../Dependencies/GLFW/glfw3.h"
#include "WindowInfo.h"
#include "IListener.h"

namespace Core
{
	class Init_GLFW
	{
	public:
		static void init(int argc, char **argv,
			const Core::WindowInfo &window,
			const Core::ContextInfo &context,
			const Core::FramebufferInfo &framebuffer);

		static void run();
		static void close();
		static void setListener(Core::IListener*& iListener);
		static GLFWwindow *getWindow();
		static const Core::WindowInfo *getWindowInfo();
		static double time;

	private:
		static void idleCallback(void);
		static void displayCallback(void);
		static void reshapeCallback(int width, int height);
		static void closeCallback(void);

		static Core::IListener *listener;
		static GLFWwindow *glfwWindow;
		static const Core::WindowInfo *windowInfo;
	};
}