#include "Core\Init\Init_GLEW.h"
#include "Core\Init\WindowInfo.h"
#include "Core\Init\Init_GLFW.h"

#include "Managers\Shader_Manager.h"
#include "Managers\Scene_Manager.h"
#include <iostream>
#include <string>
#include "Dependencies\trimesh\TriMesh.h"


//#pragma comment(linker, "/STACK:4182016")
//#pragma comment(linker, "/HEAP:4182016")

Managers::Shader_Manager *shaderManager;
GLuint program;

int main(int argc, char **argv)
{
	const glm::vec2 SCREEN_SIZE = Managers::Scene_Manager::getScreenSize();
	Core::WindowInfo window("SSAO FRR 3", SCREEN_SIZE.x, SCREEN_SIZE.y, 100, 100, true);
	Core::ContextInfo context(4, 5, true);
	std::string modelLocation = (argc > 1 ? argv[1] : "Rendering\\Models\\REF_kon.ply");

	Core::FramebufferInfo framebuffer(true, true, true, true);
	Core::Init_GLFW::init(argc, argv, window, context, framebuffer);
	glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);
	//glEnable(GL_BLEND);

	Core::IListener* scene = new Managers::Scene_Manager(modelLocation);
	Core::Init_GLFW::setListener(scene);
	Core::Init_GLFW::run();



	return 0;
}