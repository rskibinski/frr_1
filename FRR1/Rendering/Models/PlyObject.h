#pragma once
#include "Models.h"
#include "../../Managers/Controls.h"
#include "../../Managers/Shader_Manager.h"
#include <time.h>
#include "../../Dependencies/trimesh/trimesh.h"
#include <string>
#include <map>

namespace Rendering
{
	namespace Models
	{
		class PlyObject : public Model
		{
		public:
			PlyObject(std::string ifileName,
				glm::vec3 iposition,
				glm::vec3 iscale,
				glm::vec4 color,
				GLenum idrawingMode, bool iifDrawBoundingSpheres=false, bool iifDrawNormal=true,
				bool iifFrustumCulling = false);
			~PlyObject();

			void create();
			virtual void draw() override;
			virtual void update() override;
			virtual void computeIfInFrustum(Managers::ViewFrustum *vf) override;
			virtual bool isInFrustum() override;

			virtual void runOcclusionQuery() override;
			virtual int getSamplesNumber() override;

			trimesh::TriMesh *getTrimesh();
			static int c;
		private:
			time_t timer;
			trimesh::TriMesh* mesh;
			std::vector<VertexFormat> vertices;
			glm::vec3 position;
			glm::vec4 color;
			bool ifDrawBoundingSphere, ifDrawNormal, ifFrustumCulling;
			bool objectInFrustum = false;
			static std::map<std::string, std::vector<VertexFormat>*> fileMeshMap;
			static std::map<std::string, trimesh::TriMesh*> trimeshMap;
			PlyObject *boundingSphere;
			GLenum drawingMode;
			int samplesNumber = 0;
			int resultAvailable = 0;
			GLuint queryId;

			void drawNormal();
			//void drawFrustumCulling();
			void drawBoundingSphere();
		};
	}
}