#pragma once
#include <vector>

#include "../IGameObject.h"

namespace Rendering
{
	namespace Models
	{
		class Model :public IGameObject
		{
		public:
			Model();
			virtual ~Model();
			virtual void draw() override;
			virtual void update() override;
			virtual void setProgram(GLuint shaderID) override;
			virtual void destroy() override;

			virtual GLuint getVao() const override;
			virtual const std::vector<GLuint>& getVbos() const override;

		protected:
			GLuint vao;
			GLuint program;
			std::vector<GLuint> vbos;
		};
	}
}