#include "PlyObject.h"

std::map<std::string, std::vector<Rendering::VertexFormat>*> Rendering::Models::PlyObject::fileMeshMap;
std::map<std::string, trimesh::TriMesh*> Rendering::Models::PlyObject::trimeshMap;

Rendering::Models::PlyObject::PlyObject(std::string ifileName, 
	glm::vec3 iposition, 
	glm::vec3 iscale,
	glm::vec4 color,
	GLenum idrawingMode, bool iifDrawBoundingSpheres, bool iifDrawNormal, bool iifFrustumCulling)
{

	this->ifDrawBoundingSphere = iifDrawBoundingSpheres;
	this->drawingMode = idrawingMode;
	this->position = iposition;
	this->color = color;
	this->ifFrustumCulling = iifFrustumCulling;

	if (fileMeshMap.find(ifileName) == fileMeshMap.end())
	{
		mesh = trimesh::TriMesh::read(ifileName);
		mesh->need_faces();
		mesh->need_normals();
		if (ifDrawBoundingSphere || ifFrustumCulling) 
		{
			mesh->need_bsphere();
		}


		std::vector<VertexFormat> *vertices = new std::vector<VertexFormat>();
		for (auto face : mesh->faces)
		{
			trimesh::point p0 = mesh->vertices[face[0]];
			trimesh::point p1 = mesh->vertices[face[1]];
			trimesh::point p2 = mesh->vertices[face[2]];
			trimesh::point n0 = mesh->normals[face[0]];
			trimesh::point n1 = mesh->normals[face[1]];
			trimesh::point n2 = mesh->normals[face[2]];

			vertices->push_back(
				VertexFormat(
					glm::vec3(p0[0] * iscale[0], p0[1] * iscale[1], p0[2] * iscale[2]),
					glm::vec4(0.0f, 0.0f, 0.0f, 1.0f),
					n0
				));
			vertices->push_back(
				VertexFormat(
					glm::vec3(p1[0] * iscale[0], p1[1] * iscale[1], p1[2] * iscale[2]),
					glm::vec4(0.0f, 0.0f, 0.0f, 1.0f),
					n1
				));
			vertices->push_back(
				VertexFormat(
					glm::vec3(p2[0] * iscale[0], p2[1] * iscale[1], p2[2] * iscale[2]),
					glm::vec4(0.0f, 0.0f, 0.0f, 1.0f),
					n2
				));
		}

		fileMeshMap[ifileName] = vertices;
		trimeshMap[ifileName] = mesh;
	}


	this->vertices = *fileMeshMap[ifileName];

		this->mesh = trimeshMap[ifileName];
	this->ifDrawNormal = iifDrawNormal;
	if (iifFrustumCulling || iifDrawBoundingSpheres)
	{
		boundingSphere = new PlyObject("Rendering\\Models\\Cube.ply",
			glm::vec3(mesh->bsphere.center[0] + position.x, mesh->bsphere.center[1] + position.y, mesh->bsphere.center[2] + position.z),
			glm::vec3(mesh->bsphere.r),
			color,
			GL_POINTS,
			iifFrustumCulling,
			true,
			false);
		boundingSphere->setProgram(Managers::Shader_Manager::GetShader("colorShader"));
	}

}

Rendering::Models::PlyObject::~PlyObject()
{
	delete mesh;
	delete boundingSphere;
	for (auto i : Rendering::Models::PlyObject::fileMeshMap)
	{
		delete i.second;
	}
	for (auto i : Rendering::Models::PlyObject::trimeshMap)
	{
		delete i.second;
	}
}

void Rendering::Models::PlyObject::create()
{
	GLuint vao;
	GLuint verts_vbo;

	time(&timer);
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &verts_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, verts_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * vertices.size(), &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)0);

	//glEnableVertexAttribArray(1);
	//glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::normal)));

	glBindVertexArray(0);
	this->vao = vao;
	this->vbos.push_back(verts_vbo);	
	if(boundingSphere != NULL)
		boundingSphere->create();
	
	glGenQueries(1, &queryId);
}

void Rendering::Models::PlyObject::draw()
{
	if (ifDrawNormal)
	{
		drawNormal();
	}

	if	(ifDrawBoundingSphere)
	{
		drawBoundingSphere();
	}

}

void bindModelMatrix(int program, glm::vec3 position)
{
	GLuint M_MatrixID = glGetUniformLocation(program, "M");
	glm::mat4 ModelMatrix = glm::translate(glm::mat4(1.0f), position);
	glUniformMatrix4fv(M_MatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);
}

void Rendering::Models::PlyObject::drawNormal()
{
	glUseProgram(program);
	glBindVertexArray(vao);

	bindModelMatrix(this->program, this->position);

	glDrawArrays(drawingMode, 0, this->vertices.size());

	glBindVertexArray(0);
}


void Rendering::Models::PlyObject::drawBoundingSphere()
{
	boundingSphere->drawNormal();
}


void Rendering::Models::PlyObject::update()
{
}

void Rendering::Models::PlyObject::computeIfInFrustum(Managers::ViewFrustum *vf)
{
	trimesh::point sphereCenter = mesh->bsphere.center + trimesh::point(position.x, position.y, position.z);
	objectInFrustum = vf->isSphereInFrustum(sphereCenter, mesh->bsphere.r);
}

bool Rendering::Models::PlyObject::isInFrustum()
{
	return objectInFrustum;
}

void Rendering::Models::PlyObject::runOcclusionQuery()
{
	glUseProgram(program);
	glBindVertexArray(vao);

	glBeginQuery(GL_SAMPLES_PASSED, queryId);
		drawBoundingSphere();
	glEndQuery(GL_SAMPLES_PASSED);

	glBindVertexArray(0);
}

int Rendering::Models::PlyObject::getSamplesNumber()
{
	glGetQueryObjectiv(queryId, GL_QUERY_RESULT, &samplesNumber);
	return this->samplesNumber;
}

trimesh::TriMesh * Rendering::Models::PlyObject::getTrimesh()
{
	return mesh;
}
