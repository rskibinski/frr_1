#pragma once

#include <vector>
#include <iostream>
#include "../Dependencies/glew/glew.h"
#include "../Dependencies/freeglut/freeglut.h"
#include "VertexFormat.h"
#include "glm/glm.hpp"
#include "../Managers/Controls.h"
#include <glm/gtc/matrix_transform.hpp>

namespace Rendering
{
	class IGameObject
	{
	public:
		virtual ~IGameObject() = 0;
		virtual void draw() = 0;
		virtual void update() = 0;
		virtual void setProgram(GLuint shaderID) = 0;
		virtual void destroy() = 0;

		virtual void computeIfInFrustum(Managers::ViewFrustum *vf) = 0;
		virtual bool isInFrustum() = 0;

		virtual void runOcclusionQuery() = 0;
		virtual int getSamplesNumber() = 0;

		virtual GLuint getVao() const = 0;
		virtual const std::vector<GLuint>& getVbos() const = 0;
	};

	inline IGameObject::~IGameObject()
	{
	}
}