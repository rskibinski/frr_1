#ifndef  VertexFormat_H_
#define VertexFormat_H_

#pragma once

#include "glm\glm.hpp"
#include "../Dependencies/trimesh/trimesh.h"


namespace Rendering {
	struct VertexFormat
	{
		glm::vec3 position;
		glm::vec3 normal;

		VertexFormat(const glm::vec3 &pos, const glm::vec4 &col, const trimesh::point norm) {
			position = pos;
			normal = glm::vec3(norm[0], norm[1], norm[2]);
		}
	};
}

#endif
